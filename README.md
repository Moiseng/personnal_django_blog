## Start project

1. `Run command to install venv`
2. `yarn install`
3. `yarn run dev`
4. `python manage.py runserver` ( Then open http:127.0.0.1:8000)
5. You can run on a specific port with `python manage.py runserver 8081` (so here the link will be http:127.0.0.1:8081)

## Optimize image
https://www.birme.net

## Remove background image
https://www.remove.bg/

## Convert image to SVG format
https://picsvg.com/

## Convert SVG to PNG / JPG format
https://svgtopng.com/fr/