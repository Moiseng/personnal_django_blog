import result from "postcss/lib/result";

window._ = require('lodash')

/*
   Loading Jquery
 */

import $ from 'jquery';
import animations from '../../dev/js/animation/_animations'

let blog =  {};

function fetchInitParameters (formData, method, csrfTokenValue = undefined) {
    let _headers = new Headers({
        'X-CSRFToken': csrfTokenValue,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest'
    });

    return {
        method: `${method}`,
        body: JSON.stringify(formData),
        headers: _headers,
        mode: 'cors',
        cache: 'default'
    }
}


blog.postFormCreate = function () {
    let _form = document.querySelector('form[id^="post-form-"]');
    if (_form) {
        let published = document.getElementById('published');
        let getPublishedResult;

        published.addEventListener('click', function (event) {
            if (this.checked) {
                getPublishedResult = true;
            } else if (!this.checked) {
                getPublishedResult = false;
            }
        })

        // FOR UPDATE
        if (published.value === '1') {
            getPublishedResult = true;
        } else if (published.value === '0') {
            getPublishedResult = false;
        }

        let button = _form.querySelector('button[type="submit"]');
        button.addEventListener('click', function (event) {
        event.preventDefault();
        let messageParagraph = document.querySelector('.alert').length

        if (messageParagraph > 2) {
            $('.alert')[0].remove();
        }

        let title = document.getElementById('id_title').value,
            content = document.getElementById('id_content').value,
            images = document.getElementById('id_images').value,
            category = document.getElementById('id_categories').value;

        if (getPublishedResult === undefined) {
            getPublishedResult = false
        }

        let formData =  {
            title: title,
            content: content,
            images: images,
            published: getPublishedResult,
            category: parseInt(category)
        }
        let csrfTokenValue = document.querySelector('[name=csrfmiddlewaretoken]').value;

        fetch(_form.getAttribute('action'), fetchInitParameters(formData, 'POST', csrfTokenValue))
        .then((response) => response.json())
        .then(results => {
            let pElement = document.querySelector('.alert');
            for (let key in results) {
                if (key.startsWith('error')) {
                    pElement.classList.add('alert-danger')
                    pElement.innerHTML = results[key]
                } else if (key.startsWith('success')) {
                    pElement.classList.remove('alert-danger')
                    pElement.classList.add('alert-success')
                    pElement.innerHTML = results[key]

                    if (_form.getAttribute('data-current-route') === '/post/create/form/') {
                        _form.reset() // reset form fields
                    }
                    // redirect after form success
                    setTimeout(function () {
                        location.href = _form.getAttribute('data-route-redirect')
                    }, 2000)
                }
            }
        })
        .catch(error => {
            console.log('error here error ' + error)
        })
    });
    }
}

blog.categoryFormCreate = function () {
    let _form = document.getElementById('form-create-category');
    if (_form) {
        let button = _form.querySelector('button[type="submit"]');
        button.addEventListener('click', function (event) {
            event.preventDefault();
            let message_flash = document.querySelector('.alert').length;
            if (message_flash > 2) {
                $('.alert')[0].remove();
            }

            let name = document.getElementById('id_name').value;
            let csrfTokenValue = document.querySelector('[name=csrfmiddlewaretoken]').value;

            let formData = {
                name: name
            }

            fetch(_form.getAttribute('action'), fetchInitParameters(formData, 'POST', csrfTokenValue))
            .then(response => response.json())
            .then(result => {
                let pText = document.querySelector('.alert');
                for (let key in result) {
                    if (key.startsWith('error')) {
                        pText.classList.add('alert-danger');
                        pText.innerHTML = result[key];
                    } else if (key.startsWith('success')) {
                        pText.classList.add('alert-success');
                        pText.innerHTML = result[key];
                        _form.reset();
                    }
                }
                console.log(result)
            })
            .catch(error => {
            })
        })
    }
}

blog.postFilter = function () {
    let inputSearch = document.getElementById('filter_post');

    if (inputSearch) {
        let list_posts = document.getElementById('list_posts')
        inputSearch.addEventListener('keyup', function (e) {

            $('#list_posts > li').remove();

            if (this.value.length >= 3) {

                fetchPostFilter().then(result => {
                    if (result) {
                        result['data'].forEach(index => {
                            let li = document.createElement('li')
                            li.innerHTML = index.title
                            console.log(index.title)
                            list_posts.appendChild(li)
                        })
                        console.log(result)
                    }
                })
            } else if (this.value.length < 1 || this.value === '') {

                fetchPostFilter().then((results) => {
                    if (results) {
                        results['data_all'].forEach(index => {
                            let li = document.createElement('li')
                            li.innerHTML = index.title
                            console.log(index.title)
                            list_posts.appendChild(li)
                        })
                    }
                })
            } else {
                if (this.value.length > 0 && this.value.length < 3) {
                    let li = document.createElement('li')
                    li.innerHTML = 'Recherche en cours...'
                    list_posts.appendChild(li)
                }
            }
        });
    }

    async function fetchPostFilter () {

        let inputSearch = document.getElementById('filter_post'),
            csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        let data = {
            filter: inputSearch.value
        }
        const response = await fetch(inputSearch.getAttribute('data-route-filter'), fetchInitParameters(data, 'POST', csrfToken))
        return response.json();
    }
}

blog.postDelete = function () {
    let button = document.querySelector(".post-table");

    if (button) {
        button.addEventListener('click', (e) => {
            e.preventDefault();

            if (e.target.className.includes('post-delete')) {
                let form = document.getElementById("post-table")
                let item = e.target.getAttribute("data-item-id")
                let formData = {
                    id: item
                }
                let csrfTokenValue = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
                fetch(form.getAttribute('data-route-delete'), fetchInitParameters(formData, 'POST', csrfTokenValue))
                    .then((response) => response.json())
                    .then((result) => {
                        console.log(result)
                    })
            }

        })
    }
}

blog.homeSearchFilter = function () {
    let _form = document.getElementById("home-search-filter");
    if (_form) {
        let inputSearchFilter = document.getElementById("search"), divList = document.querySelector('.search-results-container');

        inputSearchFilter.addEventListener('keyup', (e) => {

            if (e.currentTarget.value.length >= 2) {
                divList.style.visibility = 'visible'
            } else {
                divList.style.visibility = 'hidden'
            }

            $('#home_search_list > li').remove();

            console.log(e.currentTarget.value)
            let formData = {
                search: e.currentTarget.value
            }, csrfTokenValue = document.querySelector('[name=csrfmiddlewaretoken]').value;

            fetch(_form.getAttribute('action'), fetchInitParameters(formData, 'POST', csrfTokenValue))
                .then(response => response.json())
                .then(results => {
                    let home_list = document.getElementById('home_search_list');
                    if (results['post_data']) {
                        results['post_data'].forEach((index) => {
                            let li = document.createElement('li'), link = document.createElement('a');
                            li.classList.add('search-results-item');
                            link.classList.add('search-results-item-link');
                            link.innerHTML = index.title;
                            link.href = `${_form.getAttribute('data-post-detail')}?title=${index.slug}`;
                            li.appendChild(link)
                            home_list.appendChild(li);
                        })

                    } else if (results['category_data']) {
                        results['category_data'].forEach((index) => {
                            let li = document.createElement('li'), link = document.createElement('a');
                            li.classList.add('search-results-item');
                            link.classList.add('search-results-item-link');
                            link.innerHTML = index.title;
                            link.innerHTML = index.name;
                            link.href = `${_form.getAttribute('data-post-category')}?name=${index.slug}`;
                            li.appendChild(link);
                            home_list.appendChild(li);
                        })
                    }
                })
        })
    }
}


$(document).ready(function () {
    blog.postFormCreate();
    blog.categoryFormCreate();
    blog.postFilter();
    blog.postDelete();
    blog.homeSearchFilter();
    animations.searchBar();
});

window.blog = blog;
window.jQuery = $;