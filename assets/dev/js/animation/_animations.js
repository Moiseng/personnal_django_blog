
let animations = {};

animations.searchBar = function () {
    let getClickableButton = document.getElementById('search-button');

    getClickableButton.addEventListener('click', (event) => {
        fadeoutEffect();
    });

    function fadeoutEffect() {
        let getSearchBarBlock = document.getElementById('nav-search'),
            getInput = document.getElementById("home-search-filter");
        setInterval(() => {
            if (getSearchBarBlock.classList.contains('display-none')) {
                getSearchBarBlock.classList.remove('display-none')
                if (getInput.style.opacity == 0) {
                    getInput.style.opacity = '1';
                }
            }
        }, 300)
    }
}


export default animations;
