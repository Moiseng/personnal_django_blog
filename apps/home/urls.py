from django.urls import path
from apps.home.views import HomePage


urlpatterns = [
    path('', HomePage.home_view, name='home'),
    path('home/search/filter/', HomePage.get_post_by_category_or_post_name_search_filter, name="home.post.search.category")
]
