import json

from django.http import JsonResponse
from django.shortcuts import render
from post.models import Post as PostModel, PostCategory


class HomePage:

    def __init__(self):
        pass

    def home_view(self):
        title = 'Mon titre'
        return render(self, 'home/home.html', {
            'title': title
        })

    def get_post_by_category_or_post_name_search_filter(self):
        if self.is_ajax() and self.method == 'POST':
            data = json.loads(self.body)
            print(data)
            if data['search'] is not None and len(data['search']) >= 2:
                post_exist = PostModel.objects.filter(title__icontains=data['search']).exists()
                category_exist = PostCategory.objects.filter(name__icontains=data['search']).exists()
                if post_exist:
                    data = PostModel.objects.filter(title__icontains=data['search']).values()
                    return JsonResponse({
                        'post_data': list(data)
                    })
                elif category_exist:
                    data = PostCategory.objects.filter(name__icontains=data['search']).values()
                    return JsonResponse({
                        'category_data': list(data)
                    })
        return JsonResponse({
            'error_form': 'An error occured'
        })
