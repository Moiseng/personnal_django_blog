from django.apps import AppConfig


class AppsHomeConfig(AppConfig):
    name = 'home'
