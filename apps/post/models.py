from django.db import models
import os


class Post(models.Model):
    """
    Post table model
    """
    title = models.CharField(max_length=100)
    content = models.TextField()
    slug = models.CharField(max_length=255)
    category = models.ForeignKey('PostCategory', null=True, blank=True, on_delete=models.DO_NOTHING)
    images = models.ImageField(upload_to=os.path.realpath('media/images/'), blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    published = models.BooleanField(default=False)

    def __str__(self):
        return self.title


class PostComment(models.Model):
    """ Post comment table """

    # Status const
    STATUS_VISIBLE = 'visible'
    STATUS_HIDDEN = 'hidden'

    # Status choices
    STATUS_CHOICES = (
        (STATUS_VISIBLE, 'Visible'),
        (STATUS_HIDDEN, 'Hidden')
    )

    author_name = models.CharField(max_length=100)
    post = models.ForeignKey('Post', on_delete=models.CASCADE)
    content = models.TextField()
    status = models.CharField(max_length=20, default=STATUS_VISIBLE, choices=STATUS_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True)


class PostCategory(models.Model):
    """ Post Category table """
    name = models.CharField(max_length=100)
    slug = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
