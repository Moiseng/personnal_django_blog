from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.template.defaultfilters import slugify
from django.core.paginator import Paginator
from apps.post.forms import CreatePostForm
from apps.post.model_helpers import handle_upload_file, get_file_name
from post.models import Post as PostModel, PostCategory
from django.db.models import Q
import json


class Post:

    def get_all_posts(self):
        """
        Retrieve all posts
        :return:
        """
        posts = PostModel.objects.all()
        return render(self, 'post/post.html', {
            'posts': posts
        })

    def post_create_form_view(self):
        categories = PostCategory.objects.all()
        form = CreatePostForm()
        return render(self, 'post/form.html', {
            'form': form,
            'categories': categories
        })

    def post_create(self, *args, **kwargs):
        if self.is_ajax() and self.method == 'POST':
            data = json.loads(self.body)
            if data['title'] is not None and len(data['title']) < 3:
                return JsonResponse({
                    'error_title_length': 'The title must have at least 3 characters'
                }, status=400)
            elif data['title'] is not None and len(data['title']) >= 3:
                check_if_title_exist = PostModel.objects.filter(title=data['title']).exists()
                if check_if_title_exist:
                    return JsonResponse({
                        'error_title_exist': f"The title {data['title']} exist ! Please provide another title"
                    }, status=400)

            if data['content'] is not None and len(data['content']) < 10:
                return JsonResponse({
                    'error_content_length': 'The content must have at least 10 characters'
                }, status=400)

            if data['images'] is not None:
                if data['category'] is not None and int(data['category']) > 0:
                    category = get_object_or_404(PostCategory, pk=int(data['category']))
                    PostModel.objects.create(title=data['title'],
                                             content=data['content'],
                                             slug=slugify(data['title']), images=get_file_name(data['images']),
                                             category=category, published=data['published'])
                else:
                    PostModel.objects.create(title=data['title'], content=data['content'],
                                             slug=slugify(data['title']), images=get_file_name(data['images']),
                                             published=data['published'])
            else:
                if data['category'] is not None and int(data['category']) > 0:
                    category = get_object_or_404(PostCategory, pk=int(data['category']))
                    PostModel.objects.create(title=data['title'],
                                             content=data['content'],
                                             slug=slugify(data['title']), images=get_file_name(data['images']),
                                             category=category, published=data['published'])
                else:
                    PostModel.objects.create(title=data['title'],
                                             content=data['content'],
                                             slug=slugify(data['title']), published=data['published'])

            return JsonResponse({
                'success': 'Le formulaire a bien été validé'
            }, status=200)
        else:
            return JsonResponse({
                'error_form': 'An error occured'
            }, status=400)

    def post_filter(self):
        """
        Filter post by post title OR category name
        :return:
        """
        if self.is_ajax() and self.method == 'POST':
            data = json.loads(self.body)
            if len(data['filter']) >= 3:
                posts = PostModel.objects.filter(Q(title__icontains=data['filter']) | Q(category__name__icontains=data['filter'])).values()
                return JsonResponse({
                    'data': list(posts)
                })
            elif not data['filter'] or len(data['filter']) < 1:
                posts = PostModel.objects.all().values()
                return JsonResponse({
                    'data_all': list(posts)
                })
        posts = PostModel.objects.all()
        return render(self, 'post/post_filter.html', {
            'posts': posts
        })

    def post_update_form_view(self, post_id):
        post = PostModel.objects.get(pk=post_id)
        categories = PostCategory.objects.all()
        return render(self, 'post/form_update.html', {
            'post': post,
            'categories': categories
        })

    def post_update(self, post_id, *args, **kwargs):
        if self.is_ajax() and self.method == 'POST':
            data = json.loads(self.body)
            print(data)
            if data['title'] is not None and len(data['title']) < 3:
                return JsonResponse({
                    'error_title_length': 'The title must have at least 3 characters'
                }, status=400)
            elif data['title'] is not None and len(data['title']) >= 3:
                get_old_title = PostModel.objects.filter(pk=post_id).values('title')
                for title in get_old_title:
                    if title['title'] != data['title']:
                        check_if_title_exists = PostModel.objects.filter(title=data['title']).exists()
                        if check_if_title_exists:
                            return JsonResponse({
                                'error_title_exist':  f"The title {data['title']} exist ! Please provide another title"
                            }, status=400)

            if data['content'] is not None and len(data['content']) < 10:
                return JsonResponse({
                    'error_content_length': 'The title must have at least 10 characters'
                }, status=400)

            if data['category'] is not None and int(data['category']) > 0:
                check_if_category_is_same = PostModel.objects.filter(pk=post_id).values('category')
                for category_value in check_if_category_is_same:
                    if int(category_value['category']) != int(data['category']):
                        PostModel.objects.filter(pk=post_id).update(title=data['title'], content=data['content'], category=int(data['category']))

            PostModel.objects.filter(pk=post_id).update(title=data['title'], content=data['content'])
            return JsonResponse({
                'success': 'Le formulaire a bien été validé'
            }, status=200)
        return JsonResponse({
            'error_form': 'An error occured'
        }, status=400)

    def post_delete(self):
        if self.is_ajax() and self.method == 'POST':
            data = json.loads(self.body)
            verify_if_post_exist = PostModel.objects.filter(pk=int(data['id'])).exists()
            if verify_if_post_exist is not True:
                return JsonResponse({
                    'error_not_exist_post': "Le post n'existe pas"
                })
            else:
                PostModel.objects.filter(pk=int(data['id'])).delete()
                return JsonResponse({
                    'success': 'Le post a bien été supprimé'
                })
        return JsonResponse({
            'error_form': 'An error occured'
        })

    def get_all_post_by_category(self):
        query_param = self.GET.get("name")
        data = PostModel.objects.filter(category__slug=query_param).all()

        paginator = Paginator(data, 1)
        page_number = self.GET.get('page')
        page_object = paginator.get_page(page_number)

        return render(self, 'post/post_all_by_category.html', {
            'posts': data,
            'category_name': query_param,
            'page_object': page_object
        })

    def get_post_detail(self):
        query_param = self.GET.get('title')
        data = PostModel.objects.filter(slug=query_param).values()
        return render(self, 'post/post_detail.html', {
            'data': data
        })
