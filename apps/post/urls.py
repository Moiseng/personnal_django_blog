from django.urls import path
from apps.post.views import Post

urlpatterns = [
    path('post/', Post.get_all_posts, name='post.all'),
    path('post/create/form/', Post.post_create_form_view, name='post.form'),
    path('post/create/validation/', Post.post_create, name="post.create"),
    path('post/filter/', Post.post_filter, name='post.filter'),
    path('post/update/form/<int:post_id>/', Post.post_update_form_view, name='post.update.form'),
    path('post/update/validation/<int:post_id>/', Post.post_update, name='post.update.validation'),
    path('post/delete/', Post.post_delete, name="post.delete"),
    path('post/search/', Post.get_all_post_by_category, name='post.by.category'),
    path('post/detail/', Post.get_post_detail, name='post.detail')
]
