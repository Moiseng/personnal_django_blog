import os


def handle_upload_file(file):
    with open(os.path.realpath('assets/dist/images/post/' + file), 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)


def get_file_name(images):
    return os.path.basename(images)
