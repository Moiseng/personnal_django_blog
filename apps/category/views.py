from django.http import JsonResponse
from django.shortcuts import render
from django.template.defaultfilters import slugify
from apps.category.forms import CreateCategory
from post.models import PostCategory
import json


class Category:

    def test(self):
        return render(self, 'category/category.html')

    def category_form_view(self):
        form = CreateCategory()
        return render(self, 'category/form.html',{
            'form': form
        })

    def category_create(self, *args, **kwargs):
        if self.is_ajax() and self.method == 'POST':
            data = json.loads(self.body)
            if data['name'] is not None and len(data['name']) < 3:
                return JsonResponse({
                    'error_name_category': 'The category name most contains at least 3 characters'
                }, status=400)
            PostCategory.objects.create(name=data['name'], slug=slugify(data['name']))
            return JsonResponse({
                'success': 'The category was successfuly added'
            }, status=200)
        else:
            return JsonResponse({
                'error_ajax_or_method': 'Something went wrong ! Try again'
            })
