from django import forms
from post.models import PostCategory


class CreateCategory(forms.ModelForm):
    class Meta:
        model = PostCategory
        fields = ('name', )
