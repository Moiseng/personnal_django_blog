from django.urls import path
from apps.category.views import Category


urlpatterns = [
    path('category', Category.test, name='category'),
    path('category/form/', Category.category_form_view, name='category.form'),
    path('category/create/', Category.category_create, name='category.create'),
]
