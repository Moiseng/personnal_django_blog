const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const ImageminPlugin = require('imagemin-webpack-plugin').default;
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ImageminMozjpeg = require('imagemin-mozjpeg');

module.exports = {
    entry: {
        'app.min.js': path.resolve(__dirname, "assets/dev/js/index.js"),
        'app.min.scss': path.resolve(__dirname, "assets/dev/sass/app.scss")
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
        new CopyWebpackPlugin({
            patterns: [
                {
                    from: 'media/images',
                    to: 'images/'
                }
            ]
        }),
        new ImageminPlugin({
            test: /\.(jpe?g|png)$/i,
            pngquant: {
                quality: '65-90',
                speed: 4
            },
            plugins: [
                ImageminMozjpeg({
                    progressive: true,
                    quality: 90
                })
            ]
        })
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: "/node_modules/",
                use: ['babel-loader']
            },
            {
                test: /\.(s[ac]|c)ss$/i,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            esModule: false,
                        }
                    },
                    {
                        loader: "css-loader",
                        options: {
                            importLoaders: 1
                        }
                    },
                    {
                        loader: "postcss-loader",
                        options: {
                            postcssOptions: {
                                plugins: function () {
                                    return [
                                        require('autoprefixer')
                                    ]
                                }
                            }
                        }
                    },
                    {
                        loader: "sass-loader"
                    }
                ]
            },
            {
                test: /\.(png|jpg)$/,
                loader: 'url-loader'
            }
        ]
    },
    output: {
        path: path.resolve(__dirname, "assets/dist"),
        filename: "[name]"
    },
}